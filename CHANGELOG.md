## [1.11.4](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/compare/v1.11.3...v1.11.4) (2023-10-04)


### Bug Fixes

* **deps:** Align deps with stripes core ([c3190bd](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/commit/c3190bd3985d4f5e9335902313ed8c2b5234a675))

## [1.11.3](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/compare/v1.11.2...v1.11.3) (2023-10-02)


### Bug Fixes

* **devtools:** remove default npm registry ([8cbc825](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/commit/8cbc825bbed6edd39050acb60ff4b98d995b2ca2))

## [1.11.2](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/compare/v1.11.1...v1.11.2) (2023-10-02)


### Bug Fixes

* **devtools:** testing the release process ([98c89ae](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/commit/98c89ae1dc8a938e7002fc4cd5d833017dd9937c))

## [1.11.1](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/compare/v1.11.0...v1.11.1) (2023-09-30)


### Bug Fixes

* **build:** Corrected gitlab-ci.yml ([fd361f6](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/commit/fd361f60abaf1a9c732bff1960b3acf7dc7c6972))

# [1.11.0](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/compare/v1.10.0...v1.11.0) (2023-09-30)


### Features

* **release:** Try to get a release ([b2e6f43](https://gitlab.com/knowledge-integration/libraries/networks/stripes-ill/commit/b2e6f43197259f07cf5c0325a67cc7147204d6a0))

# Change history for stripes-ill

## [1.0.0](https://github.com/openlibraryenvironment/stripes-ill/tree/v1.0.0) (2020-08-24)

* Initial release
