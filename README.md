# stripes-ill


This is a library of common elements used by Stripes apps for the ILL system.

This library is managed by https://www.npmjs.com/package/semantic-release - please don't manually adjust the project version. Instead merge 
onto the release branch and use "feature", "fix" and "chore" semantic commits to infer version increments.

See https://github.com/angular/angular/blob/main/CONTRIBUTING.md#commit-header for info on accepted commit types
