import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';
import { TAGS_ENDPOINT } from '@k-int/ill-ui';


const useILLTags = (options) => {
  const ky = useOkapiKy();
  const nsArray = ['ILL', 'Tags'];

  return useQuery(
    nsArray,
    () => ky.get(TAGS_ENDPOINT).json(),
    options
  );
};

export default useILLTags;
